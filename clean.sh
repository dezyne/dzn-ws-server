#!/bin/bash

# dzn-ws-server -- Dezyne components for server-side WebSocket support
# Copyright (C) 2019 Verum Software Tools B.V. <support@verum.com>
#
# This file is part of dzn-ws-server.
#
# dzn-ws-server is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# dzn-ws-server is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this dzn-ws-server.  If not, see <https://www.gnu.org/licenses/>.

rm -f js/dzn/IControl.js
rm -f js/dzn/IWebSocket.js
rm -f js/dzn/WebSocketEchoServer.js
rm -f js/dzn/WebSocketListElement.js

rm -f js/dzn/runtime.js
