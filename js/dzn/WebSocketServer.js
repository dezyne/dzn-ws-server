// dzn-ws-server -- Dezyne components for server-side WebSocket support
// Copyright (C) 2019 Verum Software Tools B.V. <support@verum.com>
//
// This file is part of dzn-ws-server.
//
// dzn-ws-server is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dzn-ws-server is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this dzn-ws-server.  If not, see <https://www.gnu.org/licenses/>.

function node_p () {return typeof module !== 'undefined';}
function have_dzn_p () {return typeof (dzn) !== 'undefined' && dzn;}

if (node_p ()) {
  // nodejs
  module.paths.unshift (__dirname);
  dzn_require = require;
  dzn = have_dzn_p () ? dzn : require (__dirname + '/runtime');
  dzn = dzn || {};
} else {
  // browser
  dzn_require = function () {return {};};
  dzn = have_dzn_p () ? dzn : {};
  /* Add to your html something like
  <script src="js/dzn/runtime.js"></script>
  <script src="js/dzn/sexp.js"></script>
  <script src="js/dzn/WebSocketServer.js"></script>
  */
}



dzn = dzn || {};

if (node_p ()) {dzn.extend (dzn, dzn_require (__dirname + '/IWebSocket'));}


dzn.WebSocketServer = function (locator, meta) {
  dzn.runtime.init (this, locator, meta);
  this._dzn.meta.ports = ['ctrl'];
  // this._dzn.flushes = true; // Foreign component does not explicitly flush client queue on out-event send





  this.ctrl = new dzn.IWebSocketServer({provides: {name: 'ctrl', component: this}, requires: {}});

  // Foreign vvvvv
  this.wss = null; // host: npm ws WebSocket.Server instance
  // Foreign ^^^^^



  this.ctrl.in.listen = function(options) {
    // Foreign vvvvv
    if (this.wss) this._dzn.rt.illegal();
    try {
      if (!this.wss) {
        this.wss = new (require ('ws').Server) (options);
        console.log('WebSocketServer: listening on - ', options);

        this.wss.on('close', function() {this.wss = null; this.ctrl.out.closed();}.bind(this));
        this.wss.on('connection', function (socket) {this.ctrl.out.connection(socket);}.bind (this));
        this.wss.on('error', function(e) {this.ctrl.out.error(e);}.bind(this));
        this.wss.on('listening', function() {this.ctrl.out.listening();}.bind(this));
      }
    }
    catch (e) {this.ctrl.out.error(e);}
    // Foreign ^^^^^
  };
  this.ctrl.in.close = function(){
    // Foreign vvvvv
    this.wss.close();
    // Foreign ^^^^^
  };



  this._dzn.rt.bind (this);
};

if (node_p ()) {
  // nodejs
  module.exports = dzn;
}

//code generator version: enterprise
