// dzn-ws-server -- Dezyne components for server-side WebSocket support
// Copyright (C) 2019 Verum Software Tools B.V. <support@verum.com>
//
// This file is part of dzn-ws-server.
//
// dzn-ws-server is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dzn-ws-server is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this dzn-ws-server.  If not, see <https://www.gnu.org/licenses/>.

function node_p () {return typeof module !== 'undefined';}
function have_dzn_p () {return typeof (dzn) !== 'undefined' && dzn;}

if (node_p ()) {
  // nodejs
  module.paths.unshift (__dirname);
  dzn_require = require;
  dzn = have_dzn_p () ? dzn : require (__dirname + '/runtime');
  dzn = dzn || {};
} else {
  // browser
  dzn_require = function () {return {};};
  dzn = have_dzn_p () ? dzn : {};
  /* Add to your html something like
  <script src="js/dzn/runtime.js"></script>
  <script src="js/dzn/sexp.js"></script>
  <script src="js/dzn/WebSocketList.js"></script>
  */
}



dzn = dzn || {};

if (node_p ()) {dzn.extend (dzn, dzn_require (__dirname + '/IWebSocket'));}
if (node_p ()) {dzn.extend (dzn, dzn_require (__dirname + '/WebSocketListElement'));}


dzn.WebSocketList = function (locator, meta) {
  dzn.runtime.init (this, locator, meta);
  this._dzn.meta.ports = ['ctrl'];
  this._dzn.flushes = true;



  this.reply_bool = null;


  this.ctrl = new dzn.IWebSocketConnection({provides: {name: 'ctrl', component: this}, requires: {}});


  // vvvvv Foreign vvvvv

  // WebSocketList contains a list of WebSocketListElement
  
  // At WebSocketList construction time the head element of that list
  // is instantiated. The provides port of the head element is bound
  // to the provides port of WebSocketList. The requires port of the
  // head element is bound by assigning function objects to the
  // in-events 'add', 'close', 'send'.
  
  // Of these the 'add' function is responsible for extending the
  // list. It constructs a new WebSocketListElement, binds the new
  // element provides port to the requires port of the previous
  // element, binds the new element requires port by assigning
  // function objects to the in-events 'add, 'close', 'send'. Finally,
  // the 'add' event on the new element provides port is invoked to
  // make it pass the WebSocket parameter to its WebSocketConnection
  // instance.

  // Note that the current implementation only extends the list when
  // there is a need for larger capacity. The current implementation
  // does not shrink the list when the WebSocket in its
  // WebSocketConnection closes. Instead the WebSocketListElelement is
  // re-used when a next 'add' event occurs. This behaviour is
  // implemented in the WebSocketMux.
  
  this.elements = []; // list of WebSocketListElement


  this.bind_last_element_requires_in_events = function () {
    this.elements[this.elements.length - 1].next.in.add =
      function(ws) {
        // extend list with next element
        this.extend_list();
        // bind: forward add(ws) call to new element
        this.elements[this.elements.length - 1].ctrl.in.add(ws);
        return true;
      }.bind(this);
    this.elements[this.elements.length - 1].next.in.close =
      function() {this._dzn.rt.illegal();}.bind(this); // mux should not call close on closed next port
    this.elements[this.elements.length - 1].next.in.send =
      function(data) {this._dzn.rt.illegal();}.bind(this); // mux should not call send on closed next port
  }

  this.create_head_element = function() {
    if (this.elements.length == 0) {
      // create, add and bind first list element
      this.elements.push(new dzn.WebSocketListElement(this._dzn.locator, {parent: this, name: 'connection[' + this.elements.length + ']'}));
      // bind: forward provides port in-events to sub-component
      this.ctrl = this.elements[0].ctrl;
      // This overwrites entire system provides port with list head element provides port.
      // At this point the owning system has not yet bound its handlers to this.ctrl.out events

      // After construction the parent component fills in
      //   this.ctrl.out and
      //   this.ctrl._dzn.meta.requires
      // by calling dzn.connect (to bind it to a requires port of another instance)
      // or by swapping out its own provides port (in case system provides port forwarded to this.ctrl)
    } else {
      console.error("List head already present. Not called at WebSocketList construction time?");
    }

    this.bind_last_element_requires_in_events();
  }.bind(this);

  this.extend_list = function() {
    if (this.elements.length == 0) {
      console.error("List head missing");
    } else {
      // create, add and bind second or later list element
      this.elements.push(new dzn.WebSocketListElement(this._dzn.locator, {parent: this, name: 'connection[' + this.elements.length + ']'}));
      // bind previous element requires port to new element provides port
      dzn.connect(this.elements[this.elements.length - 1].ctrl, this.elements[this.elements.length - 2].next);
    }

    this.bind_last_element_requires_in_events();
  }.bind(this);

  this.create_head_element();
  // ^^^^^ Foreign ^^^^^


  this._dzn.rt.bind (this);
};

if (node_p ()) {
  // nodejs
  module.exports = dzn;
}
if (node_p ()) {
  // nodejs
  dzn.extend (dzn, require ('WebSocketConnection'));
} else {
  // browser
  /* Add to your html something like
  <script src="js/dzn/WebSocketConnection.js"></script>
  */
}

//code generator version: enterprise
