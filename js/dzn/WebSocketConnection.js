// dzn-ws-server -- Dezyne components for server-side WebSocket support
// Copyright (C) 2019 Verum Software Tools B.V. <support@verum.com>
//
// This file is part of dzn-ws-server.
//
// dzn-ws-server is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// dzn-ws-server is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this dzn-ws-server.  If not, see <https://www.gnu.org/licenses/>.

function node_p () {return typeof module !== 'undefined';}
function have_dzn_p () {return typeof (dzn) !== 'undefined' && dzn;}

if (node_p ()) {
  // nodejs
  module.paths.unshift (__dirname);
  dzn_require = require;
  dzn = have_dzn_p () ? dzn : require (__dirname + '/runtime');
  dzn = dzn || {};
} else {
  // browser
  dzn_require = function () {return {};};
  dzn = have_dzn_p () ? dzn : {};
  /* Add to your html something like
  <script src="js/dzn/runtime.js"></script>
  <script src="js/dzn/sexp.js"></script>
  <script src="js/dzn/WebSocketConnection.js"></script>
  */
}



dzn = dzn || {};

if (node_p ()) {dzn.extend (dzn, dzn_require (__dirname + '/IWebSocket'));}


dzn.WebSocketConnection = function (locator, meta) {
  dzn.runtime.init (this, locator, meta);
  this._dzn.meta.ports = ['ctrl'];
  // this._dzn.flushes = true; // Foreign component does not explicitly flush client queue on out-event send



  this.reply_bool = null;


  this.ctrl = new dzn.IWebSocketConnection({provides: {name: 'ctrl', component: this}, requires: {}});

  // Foreign vvvvv
  this.connection = null; // host: npm ws WebSocket instance

  this.establishConnection = function(websocket) {
    this.connection = websocket;
    this.connection.onclose = function() {this.ctrl.out.closed(); this.connection = null;}.bind(this);
    this.connection.onerror = function(e) {this.ctrl.out.error();}.bind(this);
    this.connection.onmessage = function(event) {
      try {
        let msg = event.data;
        this.ctrl.out.message(msg);
      } catch (e) {process.nextTick(function(){this.ctrl.out.error();}.bind(this));}
    }.bind(this);
    this.connection.onopen = function() {this._dzn.rt.illegal();}.bind(this); // websocket already open, onopen shall never happen: illegal
  }.bind(this);

  this.refuseConnection = function(websocket, reason) {
    websocket.onclose = function() {};
    websocket.onmessage = function(event) {};
    websocket.onerror = function(e) {};
    websocket.send(reason);
    websocket.close();
  }.bind(this);
  // Foreign ^^^^^



  this.ctrl.in.add = function(connection){
    // Foreign vvvvv
    if (this.connection) { // already hosting an open connection
      this.refuseConnection(connection, 'Already hosting an open connection');
      return false;
    } else if (connection.constructor.name != 'WebSocket') {
      console.log('WebSocketConnection on add: Type error - not a WebSocket');
      this.refuseConnection(connection, 'Server error');
      return false;
    } else {
      this.establishConnection(connection);
      return true;
    }
    // Foreign ^^^^^
  };
  this.ctrl.in.close = function(){
    // Foreign vvvvv
    this.connection.close();
    // Foreign ^^^^^
  };
  this.ctrl.in.send = function(data){
    // Foreign vvvvv
    this.connection.send(data);
    return true; // Does npm ws WebSocket send method throw?
    // Foreign ^^^^^
  };



  this._dzn.rt.bind (this);
};

if (node_p ()) {
  // nodejs
  module.exports = dzn;
}

//code generator version: enterprise
